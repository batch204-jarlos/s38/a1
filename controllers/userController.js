const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Check if email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}

	})
}








//Controller for User Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync(<dataTobeHash>, <saltRound>)
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	});


	return newUser.save().then((user, error) => {

		if(error) {
			return false
		} else {
			return true
		}

	})
}







//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			
			return false

		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			//bcrypt.compareSync(<dataTobeCompare>, <dataFromDB>)


			// If the passwords match/result of the above code is true
			if(isPasswordCorrect) {

				console.log(result)

				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return { access: auth.createAccessToken(result)}

			// Passwords do not match	
			} else {

				return false
			}
		}

	})
}


// User Retrieval
module.exports.detailsUser = (reqBody) => {
	

	return User.findOne({_id: reqBody._id}).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "";
			return (result)

			
			// return (result)
		}

	});
}