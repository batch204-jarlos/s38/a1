const jwt = require("jsonwebtoken");
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPI";


//JSON Web Tokens

//Token Creation
module.exports.createAccessToken = (user) => {
	console.log(user)
	/*
		{
		  _id: new ObjectId("634015b461e9860026ee3c44"),
		  firstName: 'Jane',
		  lastName: 'Hufano',
		  email: 'janehufano@mail.com',
		  password: '$2b$10$jgneYUAwtGwpbfY4VwqNg.5lFOekR.v1AS3kJlxNhX54.eVlp/Bd2',
		  isAdmin: false,
		  mobileNo: '09123456789',
		  enrollments: [],
		  __v: 0
		}
	*/
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})



}

// // Token Creation

// module.exports.retrieveUserToken = (user) => {
// 	console.log(user)

// 	const userData = {
// 		id: user._id,
// 		firstName: user.firstName,
// 		lastName: user.lastName,
// 		email: user.email,
// 		isAdmin: user.isAdmin,
// 		mobileNo: user.mobileNo,
// 		enrollments: user.enrollments

// 	}
// }